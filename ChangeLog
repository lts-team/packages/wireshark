commit c31966fc59
Author: Gerald Combs <gerald@wireshark.org>
Date:   Tue Sep 6 14:59:10 2022 -0700

    Prep for 3.4.16.

commit 3a2bff1769
Author: Gerald Combs <gerald@wireshark.org>
Date:   Mon Sep 5 10:26:54 2022 -0700

    Tools: Switch make-manuf.py URLs to HTTPS.

    standards-oui.ieee.org is now served over HTTPS.

    (cherry picked from commit 4819716f1442b20b63f9804c39d7d1fc36d80f27)

commit 0664b90b35
Author: Jason Cohen <7011141-kryojenik@users.noreply.gitlab.com>
Date:   Mon Sep 5 17:31:42 2022 +0000

    f5ethtrailer: Fix possible infinite loop in legacy trailer heuristic

    (cherry picked from commit 67326401a595fffbc67eeed48eb6c55d66a55f67)

commit fbbdc3ea7d
Author: Guy Harris <gharris@sonic.net>
Date:   Sat Sep 3 23:10:36 2022 -0700

    frame: always add the "length < caplen" expert info.

    Add the "length < caplen" expert info regardless of whether the "frame"
    protocol is referenced by a packet-matching expression, just as we do
    with the "fractional time component of the absolute frame time is >=
    1000000000" expert info.

    Fixes #18312.

    (cherry picked from commit 09785f072930deb2f9b86ab100360233f24fbaa8)

commit afd056bd21
Author: Guy Harris <gharris@sonic.net>
Date:   Sat Sep 3 03:24:33 2022 -0700

    Fix bogus tvbuffs to make sure reported length >= captured length.

    A reported length less than a captured length is bogus, as you cannot
    capture more data than there is in a packet.

    Fixes #18313.

    (cherry picked from commit 022dfd56f3ff0d3d99b3e1aa0680d2b0440e5332)

commit 1dec74b553
Author: Gerald Combs <gerald@wireshark.org>
Date:   Sun Sep 4 16:34:30 2022 +0000

    [Automatic update for 2022-09-04]

    Update manuf, services enterprise numbers, translations, and other items.

commit 894692ba98
Author: Gerald Combs <gerald@wireshark.org>
Date:   Sun Aug 28 16:38:58 2022 +0000

    [Automatic update for 2022-08-28]

    Update manuf, services enterprise numbers, translations, and other items.

commit 08460a7f43
Author: Gerald Combs <gerald@wireshark.org>
Date:   Tue Aug 23 13:54:45 2022 -0700

    GitLab CI: Remove a Lintian check.

    Remove the "copyright-excludes-files-in-native-package". It was added
    in Lintian 2.57.0 and annoyingly removed without any sort of backward
    compatibility in 2.106.0. Ubuntu 22.04 / Jammy ships with Lintian
    2.114.0.

commit a595d9334e
Author: Gerald Combs <gerald@wireshark.org>
Date:   Sun Aug 21 16:34:57 2022 +0000

    [Automatic update for 2022-08-21]

    Update manuf, services enterprise numbers, translations, and other items.

commit a20d11f460
Author: huangqiangxiong <qiangxiong.huang@qq.com>
Date:   Tue Aug 2 21:24:56 2022 +0800

    Protobuf: correct the starting offsets of the field name and type tree items

    (cherry picked from commit bb6fc5bb9a2386980a5a91e3d24ae5d322afc704)

commit 51553b29c0
Author: Uli Heilmeier <uh@heilmeier.eu>
Date:   Wed Aug 17 14:33:39 2022 +0200

    BTATT: Fix bitmask for btatt.battery_power_state

    Fixes: #18267

    (cherry picked from commit 117788f6941a4c1a777d1f163da9d4fd31fb73aa)

commit eb6c44542e
Author: Guy Harris <gharris@sonic.net>
Date:   Sun Aug 14 22:50:51 2022 -0700

    Linux USB: fix incorrect values for the packet length.

    When reading memory-mapped Linux capture files, fix up the "real" length
    field, in case the file was written by a program doing a capture done
    with a version of libpcap with a bug that causes it to incorrectly set
    the "real" length for isochronous transfers.

    (cherry picked from commit 09c5183878b2a05e56ba5b5ac849785f5aafb122)

commit 6e9ec07479
Author: Guy Harris <gharris@sonic.net>
Date:   Sun Aug 14 16:12:07 2022 -0700

    frame: warn if the real length is less than the captured length.

    That should never be the case; if you slice off part of a sausage, the
    remainder of the sausage cannot be longer than the original sausage.

    Warn about that.

    (cherry picked from commit 8674eea7b0f3d5ac83460542e49f1cd64a6707ec)

commit 940ae07143
Author: Tomasz Moń <desowin@gmail.com>
Date:   Wed Jun 1 22:32:20 2022 +0200

    wiretap: Do not silently limit capture length

    Libpcap assumes that packet length is greater or equal to captured data
    length. However, due to a bug in libpcap, it was possible for libpcap to
    generate isochronous URB packets (WTAP_ENCAP_USB_LINUX_MMAPPED) with
    captured data length greater than packet length. The discrepancy comes
    from slightly different semantics in Linux kernel.

    Linux kernel usbmon packet documentation mentions:
        unsigned int length;  /* 32: Length of data (submitted or actual) */
        unsigned int len_cap; /* 36: Delivered length */

    Wireshark shows usbmon packet length as URB length (usb.urb_len) and
    len_cap as Data length (usb.data_len). For usbmon isochronous IN packets
    containing data (URB complete), usbmon length is "actual". Actual length
    is the sum of payload packets length received from device. Delivered
    length refers to the amount of data associated with usbmon packet, that
    is the isochronous descriptors and actual isochronous data. There can be
    multiple isochronous descriptors in single URB and the actual payload in
    special cases can be noncontiguous (there can be gaps).

    Libpcap when reading usbmon capture calculates packet length based on
    usbmon packet structure size (64), "actual length" and number of
    isochronous descriptors. This gives expected packet length as long as
    there are no gaps between isochronous data. If there are gaps, the
    calculated packet length will be smaller than delivered length.

    Wireshark should show the frame length and captured length as provided
    by the capture engine, even if the capture length is greater than frame
    length. Silently limiting captured length essentially hides the issue
    from the user and allows misbehaving capture engine to go unnoticed.

    Passing unmodified Frame Length and Capture Length to dissectors (and
    thus complete tvb) allows USB dissector to show all ISO Data fields
    captured on Linux usbmon interface using bugged libpcap.

    Fixes #18021

    (cherry picked from commit bf26f538c65ee37f321bc63ca9a27114914224dc)

commit dc86fa83f4
Author: Gerald Combs <gerald@wireshark.org>
Date:   Sun Aug 14 16:36:40 2022 +0000

    [Automatic update for 2022-08-14]

    Update manuf, services enterprise numbers, translations, and other items.

commit ea4a08cffa
Author: Pascal Quantin <pascal@wireshark.org>
Date:   Wed Aug 10 09:46:31 2022 +0200

    NAS 5GS:  fix decoding of T3324 IE

    T3324 IE is GPRS timer 3 type.

    (cherry picked from commit 100fe4e94eea7cf7f6823c37b1746013e1bfa2d3)

commit af642ab6f4
Author: Pascal Quantin <pascal@wireshark.org>
Date:   Mon Aug 8 10:33:22 2022 +0200

    GSM CBSP: fix dissection of Repetition Period IE

    Closes #18254

    (cherry picked from commit 4e85e0bfbe1a267650c37bce25e8260a895ca425)

commit 296c4a8ac6
Author: Gerald Combs <gerald@wireshark.org>
Date:   Sun Aug 7 16:36:30 2022 +0000

    [Automatic update for 2022-08-07]

    Update manuf, services enterprise numbers, translations, and other items.

commit 2edd275d19
Author: Gerald Combs <gerald@wireshark.org>
Date:   Fri Jun 3 13:47:41 2022 -0700

    Git+CMake: Add support for CMake presets.

    CMake 3.19 added support for CMakePresets.json and
    CMakeUserPresets.json, which let you prepopulate various configure,
    build, and test options. Add CMakeUserPresets.json to .gitignore as
    recommended by the documentation and add an example to the Developer's
    Guide. CMake uses 2-space indentation; specify that for CMake*.json in
    .editorconfig.

    (cherry picked from commit 2ac2b0670a4ae0d0ea0d481f42f23885aa8c10ce)

     Conflicts:
            .gitignore
            docbook/wsdg_src/WSDG_chapter_tools.adoc

commit 6cd392ba75
Author: Chuck Craft <bubbasnmp@gmail.com>
Date:   Wed Aug 3 10:28:42 2022 -0500

    ipx: ipxnet_hash_table clear after init causes ipx_crash

    Closes #18234

    (cherry picked from commit 566ea8ceb4296f1d1cf246174cab9be5f170e1c6)

commit 601c720e37
Author: Guy Harris <gharris@sonic.net>
Date:   Tue Aug 2 23:31:44 2022 -0700

    Qt: fix speling.

    (backported from commit 20a013a8afee9bdaa49998d43641f325ac7ffeaf)

commit 05914b0567
Author: Guy Harris <gharris@sonic.net>
Date:   Tue Aug 2 16:38:49 2022 -0700

    Make sure we don't create comment options longer than 65535 bytes.

    Check in both editcap and Wireshark to make sure that comments have
    fewer than 65536 bytes before accepting them.

    This shoudl fix #18235, although there should also be checks in
    libwiretap to catch cases where the user interface code doesn't do the
    check (it should be done in the UI so that the user gets notified
    appropriately).

    (backported from commit 71f32ef2a8bc18c65e856609a68778db2b32930d)

commit a2b53b62c1
Author: Alexis La Goutte <alexis.lagoutte@gmail.com>
Date:   Tue Jan 18 19:35:15 2022 +0000

    couchbase: fix Illegal call of proto_item_add_bitmask_tree without fields

    extra_flags can't be empty

    Fix #17890

    (cherry picked from commit ccdcc91e81c5428b6978ee9ecece2db2a36fcc6d)

commit 2f18b3effb
Author: Gerald Combs <gerald@wireshark.org>
Date:   Sun Jul 31 16:36:55 2022 +0000

    [Automatic update for 2022-07-31]

    Update manuf, services enterprise numbers, translations, and other items.

commit 73009b50a5
Author: Gerald Combs <gerald@wireshark.org>
Date:   Fri Jul 29 16:29:05 2022 -0700

    GitLab CI: Fix a path.

    Use the Visual Studio 2019 Community path for vcvarsXX.bat.

commit 2bf9da989b
Author: Gerald Combs <gerald@wireshark.org>
Date:   Fri Jul 29 10:42:48 2022 -0700

    Docs: Remove the "Last updated" footer from our HTML man pages.

    The "Last updated" footer time is the last modified time of the source
    file. We could make it reproducible using something like
    git-restore-mtime, but it's easier (and IMHO less ugly) to just remove
    the footer.

    (cherry picked from commit 8015762319b2ef97063a5fb74a79a1b2035f780e)

commit 22c7a356c2
Author: Gerald Combs <gerald@wireshark.org>
Date:   Wed Jul 27 14:36:44 2022 -0700

    Version: 3.4.15 → 3.4.16.

    [skip ci]
