include::attributes.adoc[]
:stylesheet: ws.css
:linkcss:

== Wireshark {wireshark-version} Release Notes
// Asciidoctor Syntax Quick Reference:
// https://asciidoctor.org/docs/asciidoc-syntax-quick-reference/

This is expected to be the final release of the 3.4 branch.

== What is Wireshark?

Wireshark is the world’s most popular network protocol analyzer.
It is used for troubleshooting, analysis, development and education.

== What’s New

// The Windows installers now ship with Npcap 1.31.
// They previously shipped with Npcap 1.10.

// The Windows installers now ship with USBPcap 1.5.X.0.
// They previously shipped with USBPcap 1.5.4.0.

// The Windows installers now ship with Qt 5.15.2.
// They previously shipped with Qt 5.12.1.

=== Bug Fixes

The following vulnerabilities have been fixed:

* wssalink:2022-06[]
F5 Ethernet Trailer dissector infinite loop.
wsbuglink:18307[].
// cveidlink:2022-xxxx[].
// Fixed in master: 67326401a5
// Fixed in release-3.6: 68c3d706c7
// Fixed in release-3.4: 0664b90b35
// CVSS AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:L
// CWE-835
// * f5ethtrailer: Infinite loop in legacy style dissector. wsbuglink:18307[].

The following bugs have been fixed:

//* wsbuglink:5000[]
//* wsbuglink:6000[Wireshark bug]
//* cveidlink:2014-2486[]
//* Wireshark exposed details your sordid redneck past, which were subsequently widely disseminated on social media.
// cp /dev/null /tmp/buglist.txt ; for bugnumber in `git log v3.4.4rc0.. | gsed -e 's/\(close\|fix\|resolv\)[^ ]* #/\nclose #/gI' | grep ^close | sed -e 's/close.*#\([1-9][0-9]*\).*/\1/' | sort -V -u` ; do "$(git rev-parse --show-toplevel)/tools/gen-bugnote" $bugnumber; pbpaste >> /tmp/buglist.txt; done

* USB Truncation of URB_isochronous in frames wsbuglink:18021[].

* IPX/IPX RIP: Crash on expand subtree wsbuglink:18234[].

* Qt: A file or packet comment that is too large will corrupt the pcapng file wsbuglink:18235[].

* Wrong interpretation of the cbsp.rep_period field in epan/dissectors/packet-gsm_cbsp.c wsbuglink:18254[].

* Assertion due to incorrect mask for btatt.battery_power_state.* wsbuglink:18267[].

* Qt: Expert Info dialog not showing Malformed Frame when Frame length is less than captured length wsbuglink:18312[].

* Wireshark and tshark become non-responsive when reading certain packets wsbuglink:18313[].

=== New and Updated Features

// === Removed Features and Support

//=== Removed Dissectors

// === New File Format Decoding Support

// [commaize]
// --
// --

=== New Protocol Support

There are no new protocols in this release.

=== Updated Protocol Support

// Add one protocol per line between the -- delimiters.
// ag -A1 '(define PSNAME|proto_register_protocol[^_])' $(git diff --name-only v3.4.9.. | ag packet- | sort -u)
[commaize]
--
BT ATT
CBSP
Couchbase
F5 Ethernet Trailer
Frame
NAS-5GS
Protobuf
--

=== New and Updated Capture File Support

// There is no new or updated capture file support in this release.
// Add one file type per line between the -- delimiters.
[commaize]
--
pcap, pcapng
--

// === New and Updated Capture Interfaces support

//_Non-empty section placeholder._

// === Major API Changes

== Getting Wireshark

Wireshark source code and installation packages are available from
https://www.wireshark.org/download.html.

=== Vendor-supplied Packages

Most Linux and Unix vendors supply their own Wireshark packages.
You can usually install or upgrade Wireshark using the package management system specific to that platform.
A list of third-party packages can be found on the
https://www.wireshark.org/download.html#thirdparty[download page]
on the Wireshark web site.

== File Locations

Wireshark and TShark look in several different locations for preference files, plugins, SNMP MIBS, and RADIUS dictionaries.
These locations vary from platform to platform.
You can use About → Folders to find the default locations on your system.

== Getting Help

The User’s Guide, manual pages and various other documentation can be found at
https://www.wireshark.org/docs/

Community support is available on
https://ask.wireshark.org/[Wireshark’s Q&A site]
and on the wireshark-users mailing list.
Subscription information and archives for all of Wireshark’s mailing lists can be found on
https://www.wireshark.org/lists/[the web site].

Issues and feature requests can be reported on
https://gitlab.com/wireshark/wireshark/-/issues[the issue tracker].

// Official Wireshark training and certification are available from
// https://www.wiresharktraining.com/[Wireshark University].

== Frequently Asked Questions

A complete FAQ is available on the
https://www.wireshark.org/faq.html[Wireshark web site].
