From c84da69dae35647774ce96c78e39bed6171291b8 Mon Sep 17 00:00:00 2001
From: Gerald Combs <gerald@wireshark.org>
Date: Tue, 13 Dec 2022 12:56:16 -0800
Subject: NCP: Add offset overflow checking.

Add and use check_offset_addition, which adds an expert item and throws
an exception if we're about to overflow our offset.

Fixes #18720

(cherry picked from commit dcf00fc3bdbd0884208b017b30d387d0fc0d2b93)
---
 epan/dissectors/.editorconfig      |   5 +
 epan/dissectors/packet-ncp2222.inc | 141 +++++++++++++++--------------
 tools/ncp2222.py                   |   2 +
 3 files changed, 82 insertions(+), 66 deletions(-)

diff --git a/epan/dissectors/.editorconfig b/epan/dissectors/.editorconfig
index c92cac2d31..a65560703b 100644
--- a/epan/dissectors/.editorconfig
+++ b/epan/dissectors/.editorconfig
@@ -4,6 +4,11 @@
 # https://editorconfig.org/
 #
 
+# packet-ncp2222.inc
+[*.inc]
+indent_style = space
+indent_size = 4
+
 [file-file.[ch]]
 indent_style = tab
 indent_size = tab
diff --git a/epan/dissectors/packet-ncp2222.inc b/epan/dissectors/packet-ncp2222.inc
index 63039e1a82..d0738a11df 100644
--- a/epan/dissectors/packet-ncp2222.inc
+++ b/epan/dissectors/packet-ncp2222.inc
@@ -2042,6 +2042,15 @@ typedef struct {
 static wmem_map_t *ncp_req_hash = NULL;
 static wmem_map_t *ncp_req_eid_hash = NULL;
 
+static guint32 check_offset_addition(guint32 offset, guint32 value, proto_tree *tree, packet_info *pinfo, tvbuff_t *tvb)
+{
+    if (offset > G_MAXUINT32 - value) {
+        proto_tree_add_expert_format(tree, pinfo, &ei_ncp_value_too_large, tvb, 0, 0, "Offset value too large: %u", value);
+        THROW(ReportedBoundsError);
+    }
+    return offset + value;
+}
+
 /* Hash Functions */
 static gboolean
 ncp_equal(gconstpointer v, gconstpointer v2)
@@ -3067,7 +3076,7 @@ print_nds_values(proto_tree *vtree, packet_info* pinfo, tvbuff_t *tvb, guint32 s
                 vvalues->vstring = get_string(tvb, voffset, value1);
                 proto_tree_add_string(nvtree, hf_value_string, tvb, voffset,
                                       value1, vvalues->vstring);
-                voffset = voffset + value1;
+                voffset = check_offset_addition(voffset, value1, nvtree, pinfo, tvb);
                 voffset += align_4(tvb, voffset);
             }
             break;
@@ -3161,7 +3170,7 @@ print_nds_values(proto_tree *vtree, packet_info* pinfo, tvbuff_t *tvb, guint32 s
             vvalues->vstring = get_string(tvb, voffset, value1);
             proto_tree_add_string(nvtree, hf_value_string, tvb, voffset,
                                   value1, vvalues->vstring);
-            voffset = voffset + value1;
+            voffset = check_offset_addition(voffset, value1, nvtree, pinfo, tvb);
             voffset += align_4(tvb, voffset);
             break;
         case 0x0000000c:        /* Network Address */
@@ -3208,7 +3217,7 @@ print_nds_values(proto_tree *vtree, packet_info* pinfo, tvbuff_t *tvb, guint32 s
             default:
                 break;
             }
-            voffset = voffset + value3;
+            voffset = check_offset_addition(voffset, value3, nvtree, pinfo, tvb);
             voffset += align_4(tvb, voffset);
             break;
         case 0x0000000f:        /* File System Path */
@@ -3224,14 +3233,14 @@ print_nds_values(proto_tree *vtree, packet_info* pinfo, tvbuff_t *tvb, guint32 s
             vvalues->vstring = get_string(tvb, voffset, value3);
             proto_tree_add_string(nvtree, hf_value_string, tvb, voffset,
                                   value3, vvalues->vstring);
-            voffset = voffset+value3;
+            voffset = check_offset_addition(voffset, value3, nvtree, pinfo, tvb);
             voffset += align_4(tvb, voffset);
             value4 = tvb_get_letohl(tvb, voffset); /* Length of Path name */
             voffset = voffset+4;
             vvalues->vstring = get_string(tvb, voffset, value4);
             proto_tree_add_string(nvtree, hf_value_string, tvb, voffset,
                                   value4, vvalues->vstring);
-            voffset = voffset+value4;
+            voffset = check_offset_addition(voffset, value4, nvtree, pinfo, tvb);
             voffset += align_4(tvb, voffset);
             break;
         case 0x00000010:        /* Replica Pointer */
@@ -3242,7 +3251,7 @@ print_nds_values(proto_tree *vtree, packet_info* pinfo, tvbuff_t *tvb, guint32 s
             vvalues->vstring = get_string(tvb, voffset, value2);
             proto_tree_add_string(nvtree, hf_value_string, tvb, voffset,
                                   value2, vvalues->vstring);
-            voffset = voffset+value2;
+            voffset = check_offset_addition(voffset, value2, nvtree, pinfo, tvb);
             voffset += align_4(tvb, voffset);
             proto_tree_add_item(nvtree, hf_replica_type, tvb, voffset, 2, ENC_LITTLE_ENDIAN);
             voffset = voffset+2;
@@ -3306,7 +3315,7 @@ print_nds_values(proto_tree *vtree, packet_info* pinfo, tvbuff_t *tvb, guint32 s
                 default:
                     break;
                 }
-                voffset = voffset + value5;
+                voffset = check_offset_addition(voffset, value5, adtree, pinfo, tvb);
             }
             voffset += align_4(tvb, voffset);
             break;
@@ -3325,14 +3334,14 @@ print_nds_values(proto_tree *vtree, packet_info* pinfo, tvbuff_t *tvb, guint32 s
             {
                 entry_rights=FALSE;
             }
-            voffset = voffset + value2;
+            voffset = check_offset_addition(voffset, value2, nvtree, pinfo, tvb);
             voffset += align_4(tvb, voffset);
             value3 = tvb_get_letohl(tvb, voffset);
             voffset = voffset + 4;
             vvalues->vstring = get_string(tvb, voffset, value3); /* Unicode Subject Name */
             proto_tree_add_string(nvtree, hf_value_string, tvb, voffset,
                                   value3, vvalues->vstring);
-            voffset = voffset + value3;
+            voffset = check_offset_addition(voffset, value3, nvtree, pinfo, tvb);
             voffset += align_4(tvb, voffset);
             /* Entry or Attribute Privileges */
             if (entry_rights) {
@@ -3584,7 +3593,7 @@ print_es_type(proto_tree *estree, tvbuff_t *tvb, nds_val *values, guint32 vtype,
         values->vstring = get_string(tvb, ioffset, value2);
         proto_tree_add_string(estree, hf_mv_string, tvb, ioffset,
                               value2, values->vstring);
-        values->voffset=ioffset + value2;
+        values->voffset = check_offset_addition(ioffset, value2, estree, NULL, tvb);
         ioffset = values->voffset;
         ioffset += align_4(tvb, ioffset);
         break;
@@ -3608,7 +3617,7 @@ print_es_type(proto_tree *estree, tvbuff_t *tvb, nds_val *values, guint32 vtype,
             values->vstring = get_string(tvb, ioffset, value2);
             proto_tree_add_string_format(nestree, hf_mv_string, tvb, ioffset,
                                          value2, values->vstring, "Delimiter ->%s", values->vstring);
-            ioffset=ioffset + value2;
+            ioffset = check_offset_addition(ioffset, value2, nestree, NULL, tvb);
             ioffset += align_4(tvb, ioffset);
             value3 = tvb_get_letohl(tvb, ioffset);
             ioffset = ioffset + 4;
@@ -3689,7 +3698,7 @@ print_es_type(proto_tree *estree, tvbuff_t *tvb, nds_val *values, guint32 vtype,
             values->vstring = get_string(tvb, ioffset, value2);
             proto_tree_add_string_format(nestree, hf_mv_string, tvb, ioffset,
                                          value2, values->vstring, "Delimiter ->%s", values->vstring);
-            ioffset=ioffset + value2;
+            ioffset = check_offset_addition(ioffset, value2, nestree, NULL, tvb);
             ioffset += align_4(tvb, ioffset);
             value3 = tvb_get_letohl(tvb, ioffset);
             ioffset = ioffset + 4;
@@ -3800,7 +3809,7 @@ print_es_type(proto_tree *estree, tvbuff_t *tvb, nds_val *values, guint32 vtype,
                 default:
                     break;
                 }
-                ioffset = ioffset + value3;
+                ioffset = check_offset_addition(ioffset, value3, sub1tree, NULL, tvb);
                 ioffset += align_4(tvb, ioffset);
             }
 
@@ -3825,14 +3834,14 @@ print_es_type(proto_tree *estree, tvbuff_t *tvb, nds_val *values, guint32 vtype,
             values->vstring = get_string(tvb, ioffset, value2);
             proto_tree_add_string_format(nestree, hf_mv_string, tvb, ioffset,
                                          value2, values->vstring, "Delimiter ->%s", values->vstring);
-            ioffset=ioffset + value2;
+            ioffset = check_offset_addition(ioffset, value2, nestree, NULL, tvb);
             ioffset += align_4(tvb, ioffset);
             value3 = tvb_get_letohl(tvb, ioffset);
             ioffset = ioffset + 4;
             values->vstring = get_string(tvb, ioffset, value3);
             proto_tree_add_string(nestree, hf_mv_string, tvb, ioffset,
                                   value3, values->vstring);
-            values->voffset=ioffset + value3;
+            values->voffset=check_offset_addition(ioffset, value3, nestree, NULL, tvb);
             ioffset = values->voffset;
             ioffset += align_4(tvb, ioffset);
             break;
@@ -3865,14 +3874,14 @@ print_es_type(proto_tree *estree, tvbuff_t *tvb, nds_val *values, guint32 vtype,
             values->vstring = get_string(tvb, ioffset, value4);
             proto_tree_add_string(nestree, hf_mv_string, tvb, ioffset,
                                   value4, values->vstring);
-            ioffset=ioffset + value4;
+            ioffset = check_offset_addition(ioffset, value4, nestree, NULL, tvb);
             ioffset += align_4(tvb, ioffset);
             value5 = tvb_get_letohl(tvb, ioffset);   /* RDN */
             ioffset = ioffset + 4;
             values->vstring = get_string(tvb, ioffset, value5);
             proto_tree_add_string(nestree, hf_rdn_string, tvb, ioffset,
                                   value5, values->vstring);
-            ioffset=ioffset + value5;
+            ioffset = check_offset_addition(ioffset, value5, nestree, NULL, tvb);
             ioffset += align_4(tvb, ioffset);
             break;
         case 5: /* GUID */
@@ -3915,14 +3924,14 @@ print_es_type(proto_tree *estree, tvbuff_t *tvb, nds_val *values, guint32 vtype,
         values->vstring = get_string(tvb, ioffset, value3);
         proto_tree_add_string(sub1tree, hf_mv_string, tvb, ioffset,
                               value3, values->vstring);
-        ioffset=ioffset + value3;
+        ioffset = check_offset_addition(ioffset, value3, sub1tree, NULL, tvb);
         ioffset += align_4(tvb, ioffset);
         value4 = tvb_get_letohl(tvb, ioffset);   /* RDN */
         ioffset = ioffset + 4;
         values->vstring = get_string(tvb, ioffset, value4);
         proto_tree_add_string(sub1tree, hf_rdn_string, tvb, ioffset,
                               value4, values->vstring);
-        ioffset=ioffset + value4;
+        ioffset=check_offset_addition(ioffset, value4, sub1tree, NULL, tvb);
         ioffset += align_4(tvb, ioffset);
         break;
     case 5: /* GUID */
@@ -3992,7 +4001,7 @@ process_search_expression(proto_tree *it_tree, tvbuff_t *tvb, nds_val *values)
         values->vstring = get_string(tvb, ioffset, values->vvalue);
         proto_tree_add_string(it_tree, hf_mv_string, tvb, ioffset,
                               values->vvalue, values->vstring);
-        ioffset = ioffset + values->vvalue;
+        ioffset = check_offset_addition(ioffset, values->vvalue, it_tree, NULL, tvb);
         /* end of DCWPutAttribute */
         break;
 
@@ -4186,7 +4195,7 @@ process_search_match(proto_tree *it_tree, tvbuff_t *tvb, nds_val *values)
     values->vstring = get_string(tvb, ioffset, values->vvalue);
     proto_tree_add_string(it_tree, hf_mv_string, tvb, ioffset,
             values->vvalue, values->vstring);
-    ioffset = ioffset + values->vvalue;
+    ioffset = check_offset_addition(ioffset, values->vvalue, it_tree, NULL, tvb);
     /* end of DCWPutAttribute */
 
     ioffset += align_4(tvb, ioffset);
@@ -4365,7 +4374,7 @@ process_entry_info(proto_tree *it_tree, tvbuff_t *tvb, nds_val *values)
         proto_tree_add_string_format(it_tree, hf_value_string, tvb, ioffset,
                 values->vvalue, values->vstring,
                 "Base Class: - %s", values->vstring);
-        ioffset = ioffset+values->vvalue;
+        ioffset = check_offset_addition(ioffset, values->vvalue, it_tree, NULL, tvb);
         ioffset += align_4(tvb, ioffset);
     }
     if (iter_flags & DSI_ENTRY_RDN) { /* Relative Distinguished Name */
@@ -4375,7 +4384,7 @@ process_entry_info(proto_tree *it_tree, tvbuff_t *tvb, nds_val *values)
         proto_tree_add_string_format(it_tree, hf_value_string, tvb, ioffset,
                  values->vvalue, values->vstring,
                  "Relative Distinguished Name - %s", values->vstring);
-        ioffset = ioffset+values->vvalue;
+        ioffset = check_offset_addition(ioffset, values->vvalue, it_tree, NULL, tvb);
         ioffset += align_4(tvb, ioffset);
     }
     if (iter_flags & DSI_ENTRY_DN) { /* Distinguished Name */
@@ -4385,7 +4394,7 @@ process_entry_info(proto_tree *it_tree, tvbuff_t *tvb, nds_val *values)
         proto_tree_add_string_format(it_tree, hf_value_string, tvb, ioffset,
                  values->vvalue, values->vstring,
                  "Distinguished Name - %s", values->vstring);
-        ioffset = ioffset+values->vvalue;
+        ioffset = check_offset_addition(ioffset, values->vvalue, it_tree, NULL, tvb);
         ioffset += align_4(tvb, ioffset);
     }
     if (iter_flags & DSI_PARTITION_ROOT_DN) { /* Root Distinguished Name */
@@ -4395,7 +4404,7 @@ process_entry_info(proto_tree *it_tree, tvbuff_t *tvb, nds_val *values)
         proto_tree_add_string_format(it_tree, hf_value_string, tvb, ioffset,
                  values->vvalue, values->vstring,
                  "Root Distinguished Name - %s", values->vstring);
-        ioffset = ioffset+values->vvalue;
+        ioffset = check_offset_addition(ioffset, values->vvalue, it_tree, NULL, tvb);
         ioffset += align_4(tvb, ioffset);
     }
     if (iter_flags & DSI_PARENT_DN) { /* Parent Distinguished Name */
@@ -4405,7 +4414,7 @@ process_entry_info(proto_tree *it_tree, tvbuff_t *tvb, nds_val *values)
         proto_tree_add_string_format(it_tree, hf_value_string, tvb, ioffset,
                  values->vvalue, values->vstring,
                  "Parent Distinguished Name - %s", values->vstring);
-        ioffset = ioffset+values->vvalue;
+        ioffset = check_offset_addition(ioffset, values->vvalue, it_tree, NULL, tvb);
         ioffset += align_4(tvb, ioffset);
     }
     if (iter_flags & DSI_PURGE_TIME) { /* Purge Time */
@@ -4420,7 +4429,7 @@ process_entry_info(proto_tree *it_tree, tvbuff_t *tvb, nds_val *values)
         values->vstring = get_string(tvb, ioffset, values->vvalue);
         proto_tree_add_string(it_tree, hf_deref_base, tvb, ioffset,
                 values->vvalue, values->vstring);
-        ioffset = ioffset + values->vvalue;
+        ioffset = check_offset_addition(ioffset, values->vvalue, it_tree, NULL, tvb);
     }
     if (iter_flags & DSI_REPLICA_NUMBER) { /* Replica Number */
         proto_tree_add_item_ret_uint(it_tree, hf_replica_number, tvb, ioffset,
@@ -4566,7 +4575,7 @@ dissect_nds_iterator(proto_tree *it_tree, tvbuff_t *tvb, packet_info *pinfo, gui
                     values.vstring = get_string(tvb, ioffset, values.vvalue);
                     proto_tree_add_string(it_subtree, hf_mv_string, tvb, ioffset,
                             values.vvalue, values.vstring);
-                    ioffset = ioffset + values.vvalue;
+                    ioffset = check_offset_addition(ioffset, values.vvalue, it_subtree, pinfo, tvb);
 #endif
                     /* end of DCWPutAttribute */
 
@@ -4609,7 +4618,7 @@ dissect_nds_iterator(proto_tree *it_tree, tvbuff_t *tvb, packet_info *pinfo, gui
                 values.vstring = get_string(tvb, ioffset, values.vvalue);
                 proto_tree_add_string(it_subtree, hf_mv_string, tvb, ioffset,
                         values.vvalue, values.vstring);
-                ioffset = ioffset + values.vvalue;
+                ioffset = check_offset_addition(ioffset, values.vvalue, it_subtree, pinfo, tvb);
                 ioffset += align_4(tvb, ioffset);
                 break;
             default:
@@ -4795,7 +4804,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
             values->vstring = get_string(tvb, ioffset, value1);
             proto_tree_add_string(ntree, hf_mv_string, tvb, ioffset,
                                   value1, values->vstring);
-            ioffset = ioffset + value1;
+            ioffset = check_offset_addition(ioffset, value1, ntree, pinfo, tvb);
         }
         break;
 
@@ -4811,7 +4820,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                 values->vstring = get_string(tvb, ioffset, value1);
                 proto_tree_add_string(ntree, hf_mv_string, tvb, ioffset,
                                       value1, values->vstring);
-                ioffset = ioffset + value1;
+                ioffset = check_offset_addition(ioffset, value1, ntree, pinfo, tvb);
             }
             break;
         case 1:
@@ -4844,7 +4853,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                 values->vstring = get_string(tvb, ioffset, value2);
                 proto_tree_add_string(ntree, hf_mv_string, tvb, ioffset,
                                       value2, values->vstring);
-                values->voffset=ioffset + value2;
+                values->voffset = check_offset_addition(ioffset, value2, ntree, pinfo, tvb);
                 ioffset += value2;
                 ioffset += align_4(tvb, ioffset);
                 value3 = tvb_get_letohl(tvb, ioffset);
@@ -4873,7 +4882,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                 values->vstring = get_string(tvb, ioffset, value2);
                 proto_tree_add_string(ntree, hf_mv_string, tvb, ioffset,
                                       value2, values->vstring);
-                ioffset = ioffset + value2;
+                ioffset = check_offset_addition(ioffset, value2, ntree, pinfo, tvb);
                 ioffset += align_4(tvb, ioffset);
                 value3 = tvb_get_letohl(tvb, ioffset);
 
@@ -4922,7 +4931,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                 values->vstring = get_string(tvb, ioffset, value2);
                 proto_tree_add_string(ntree, hf_mv_string, tvb, ioffset,
                                       value2, values->vstring);
-                ioffset = ioffset + value2;
+                ioffset = check_offset_addition(ioffset, value2, ntree, pinfo, tvb);
                 value3 = tvb_get_letohl(tvb, ioffset);
 
                 proto_tree_add_uint_format(ntree, hf_nds_uint32value, tvb, ioffset, 4,
@@ -4970,7 +4979,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
             value1 = tvb_get_letohl(tvb, ioffset);
             proto_tree_add_uint_format(ntree, hf_nds_uint32value, tvb, ioffset,
                                        4, value1, "Value %d", value1);
-            ioffset = ioffset + value1;
+            ioffset = check_offset_addition(ioffset, value1, ntree, pinfo, tvb);
         }
         break;
 
@@ -4982,7 +4991,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
             values->vstring = get_string(tvb, ioffset, value1);
             proto_tree_add_string(ntree, hf_mv_string, tvb, ioffset,
                                   value1, values->vstring);
-            ioffset = ioffset + value1;
+            ioffset = check_offset_addition(ioffset, value1, ntree, pinfo, tvb);
             ioffset += align_4(tvb, ioffset);
             values->voffset = ioffset;
             print_nds_values(ntree, pinfo, tvb, 9, values);
@@ -5000,7 +5009,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
             proto_tree_add_string(ntree, hf_nds_base, tvb, ioffset,
                                   value1, values->vstring);
             values->mvtype = MVTYPE_ATTR_REQUEST;
-            ioffset = ioffset + value1;
+            ioffset = check_offset_addition(ioffset, value1, ntree, pinfo, tvb);
         }
         break;
 
@@ -5061,7 +5070,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                         temp_values.vstring = get_string(tvb, ioffset, value1);
                         proto_tree_add_string(ntree, hf_nds_name, tvb, ioffset,
                                               value1, temp_values.vstring);
-                        ioffset = ioffset + value1;
+                        ioffset = check_offset_addition(ioffset, value1, ntree, pinfo, tvb);
                         break;
                     case 0x00000080:                /*p3values.bit8 = "Replica Type & State"*/
                         value1 = tvb_get_letohl(tvb, ioffset);
@@ -5108,7 +5117,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
             temp_values.vstring = get_string(tvb, ioffset, value2);   /* Name of Attribute */
             proto_tree_add_string(ntree, hf_mv_string, tvb, ioffset,
                                   value2, temp_values.vstring);
-            ioffset = ioffset + value2;
+            ioffset = check_offset_addition(ioffset, value2, ntree, pinfo, tvb);
             ioffset += align_4(tvb, ioffset);
             if(value1 != 1 && value1 != 6)
             {
@@ -5180,7 +5189,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                 default:
                     break;
                 }
-                ioffset = ioffset + value3;
+                ioffset = check_offset_addition(ioffset, value3, atree, pinfo, tvb);
                 ioffset += align_4(tvb, ioffset);
             }
 
@@ -5232,7 +5241,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
             default:
                 break;
             }
-            ioffset = ioffset + value3;
+            ioffset = check_offset_addition(ioffset, value3, atree, pinfo, tvb);
             ioffset += align_4(tvb, ioffset);
         }
         break;
@@ -5423,7 +5432,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                         temp_values.vstring = get_string(tvb, ioffset, value1);
                         proto_tree_add_string(ntree, hf_nds_base, tvb, ioffset,
                                               value1, temp_values.vstring);
-                        ioffset = ioffset + value1;
+                        ioffset = check_offset_addition(ioffset, value1, ntree, pinfo, tvb);
                         break;
                     case DSI_ENTRY_RDN:               /* Relative Distinguished Name */
                         value1 = tvb_get_letohl(tvb, ioffset);
@@ -5431,7 +5440,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                         temp_values.vstring = get_string(tvb, ioffset, value1);
                         proto_tree_add_string(ntree, hf_nds_relative_dn, tvb, ioffset,
                                               value1, temp_values.vstring);
-                        ioffset = ioffset + value1;
+                        ioffset = check_offset_addition(ioffset, value1, ntree, pinfo, tvb);
                         break;
                     case DSI_ENTRY_DN:                /* Distinguished Name */
                         value1 = tvb_get_letohl(tvb, ioffset);
@@ -5439,7 +5448,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                         temp_values.vstring = get_string(tvb, ioffset, value1);
                         proto_tree_add_string(ntree, hf_nds_name, tvb, ioffset,
                                               value1, temp_values.vstring);
-                        ioffset = ioffset + value1;
+                        ioffset = check_offset_addition(ioffset, value1, ntree, pinfo, tvb);
                         break;
                     case DSI_PARTITION_ROOT_DN:       /* Root Distinguished Name */
                         value1 = tvb_get_letohl(tvb, ioffset);
@@ -5447,7 +5456,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                         temp_values.vstring = get_string(tvb, ioffset, value1);
                         proto_tree_add_string(ntree, hf_nds_name, tvb, ioffset,
                                               value1, temp_values.vstring);
-                        ioffset = ioffset + value1;
+                        ioffset = check_offset_addition(ioffset, value1, ntree, pinfo, tvb);
                         break;
                     case DSI_PARENT_DN:               /* Parent Distinguished Name */
                         value1 = tvb_get_letohl(tvb, ioffset);
@@ -5455,7 +5464,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                         temp_values.vstring = get_string(tvb, ioffset, value1);
                         proto_tree_add_string(ntree, hf_nds_name, tvb, ioffset,
                                               value1, temp_values.vstring);
-                        ioffset = ioffset + value1;
+                        ioffset = check_offset_addition(ioffset, value1, ntree, pinfo, tvb);
                         break;
                     case DSI_PURGE_TIME:              /* Purge Time */
                         ns.secs = tvb_get_letohl(tvb, ioffset);   /* Seconds */
@@ -5471,7 +5480,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                         temp_values.vstring = get_string(tvb, ioffset, value1);
                         proto_tree_add_string(ntree, hf_deref_base, tvb, ioffset,
                                               value1, temp_values.vstring);
-                        ioffset = ioffset + value1;
+                        ioffset = check_offset_addition(ioffset, value1, ntree, pinfo, tvb);
                         break;
                     default:
                         break;
@@ -5539,7 +5548,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
             sub1item = proto_tree_add_string(ntree, hf_nds_base_class, tvb, ioffset,
                                              value1, temp_values.vstring);
             sub1tree = proto_item_add_subtree(sub1item, ett_nds);
-            ioffset = ioffset + value1;
+            ioffset = check_offset_addition(ioffset, value1, sub1tree, pinfo, tvb);
             ioffset += align_4(tvb, ioffset);
             if(values->vflags != 0)
             {
@@ -5583,7 +5592,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                         temp_values.vstring = get_string(tvb, ioffset, value2);
                         proto_tree_add_string(sub2tree, hf_nds_super, tvb, ioffset,
                                               value2, temp_values.vstring);
-                        ioffset = ioffset + value2;
+                        ioffset = check_offset_addition(ioffset, value2, sub2tree, pinfo, tvb);
                         ioffset += align_4(tvb, ioffset);
                     }
                     value1 = tvb_get_letohl(tvb, ioffset);  /* Containment Classes */
@@ -5598,7 +5607,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                         temp_values.vstring = get_string(tvb, ioffset, value2);
                         proto_tree_add_string(sub2tree, hf_nds_base_class, tvb, ioffset,
                                               value2, temp_values.vstring);
-                        ioffset = ioffset + value2;
+                        ioffset = check_offset_addition(ioffset, value2, sub2tree, pinfo, tvb);
                         ioffset += align_4(tvb, ioffset);
                     }
                     value1 = tvb_get_letohl(tvb, ioffset);  /* Naming Attributes */
@@ -5613,7 +5622,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                         temp_values.vstring = get_string(tvb, ioffset, value2);
                         proto_tree_add_string(sub2tree, hf_mv_string, tvb, ioffset,
                                               value2, temp_values.vstring);
-                        ioffset = ioffset + value2;
+                        ioffset = check_offset_addition(ioffset, value2, sub2tree, pinfo, tvb);
                         ioffset += align_4(tvb, ioffset);
                     }
                     value1 = tvb_get_letohl(tvb, ioffset);  /* Mandatory Attributes */
@@ -5628,7 +5637,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                         temp_values.vstring = get_string(tvb, ioffset, value2);
                         proto_tree_add_string(sub2tree, hf_mv_string, tvb, ioffset,
                                               value2, temp_values.vstring);
-                        ioffset = ioffset + value2;
+                        ioffset = check_offset_addition(ioffset, value2, sub2tree, pinfo, tvb);
                         ioffset += align_4(tvb, ioffset);
                     }
                     value1 = tvb_get_letohl(tvb, ioffset);  /* Optional Attributes */
@@ -5644,7 +5653,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                         temp_values.vstring = get_string(tvb, ioffset, value2);
                         proto_tree_add_string(sub2tree, hf_mv_string, tvb, ioffset,
                                               value2, temp_values.vstring);
-                        ioffset = ioffset + value2;
+                        ioffset = check_offset_addition(ioffset, value2, sub2tree, pinfo, tvb);
                         if(tvb_captured_length_remaining(tvb, ioffset) < 4 )
                         {
                             break;
@@ -5699,7 +5708,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                         temp_values.vstring = get_string(tvb, ioffset, value2);
                         proto_tree_add_string(sub2tree, hf_nds_acl_protected_attribute, tvb, ioffset,
                                               value2, temp_values.vstring);
-                        ioffset = ioffset + value2;
+                        ioffset = check_offset_addition(ioffset, value2, sub2tree, pinfo, tvb);
                         ioffset += align_4(tvb, ioffset);
 
                         value2 = tvb_get_letohl(tvb, ioffset);
@@ -5707,7 +5716,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                         temp_values.vstring = get_string(tvb, ioffset, value2);
                         proto_tree_add_string(sub2tree, hf_nds_acl_subject, tvb, ioffset,
                                               value2, temp_values.vstring);
-                        ioffset = ioffset + value2;
+                        ioffset = check_offset_addition(ioffset, value2, sub2tree, pinfo, tvb);
                         ioffset += align_4(tvb, ioffset);
 
                         proto_tree_add_item(sub2tree, hf_nds_acl_privileges, tvb, ioffset,
@@ -5771,7 +5780,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                         temp_values.vstring = get_string(tvb, ioffset, value2);
                         proto_tree_add_string(sub2tree, hf_nds_super, tvb, ioffset,
                                               value2, temp_values.vstring);
-                        ioffset = ioffset + value2;
+                        ioffset = check_offset_addition(ioffset, value2, sub2tree, pinfo, tvb);
                         ioffset += align_4(tvb, ioffset);
                     }
                     value1 = tvb_get_letohl(tvb, ioffset);  /* Containment Classes */
@@ -5786,7 +5795,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                         temp_values.vstring = get_string(tvb, ioffset, value2);
                         proto_tree_add_string(sub2tree, hf_nds_base_class, tvb, ioffset,
                                               value2, temp_values.vstring);
-                        ioffset = ioffset + value2;
+                        ioffset = check_offset_addition(ioffset, value2, sub2tree, pinfo, tvb);
                         ioffset += align_4(tvb, ioffset);
                     }
                     value1 = tvb_get_letohl(tvb, ioffset);  /* Naming Attributes */
@@ -5801,7 +5810,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                         temp_values.vstring = get_string(tvb, ioffset, value2);
                         proto_tree_add_string(sub2tree, hf_mv_string, tvb, ioffset,
                                               value2, temp_values.vstring);
-                        ioffset = ioffset + value2;
+                        ioffset = check_offset_addition(ioffset, value2, sub2tree, pinfo, tvb);
                         ioffset += align_4(tvb, ioffset);
                     }
                     value1 = tvb_get_letohl(tvb, ioffset);  /* Mandatory Attributes */
@@ -5816,7 +5825,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                         temp_values.vstring = get_string(tvb, ioffset, value2);
                         proto_tree_add_string(sub2tree, hf_mv_string, tvb, ioffset,
                                               value2, temp_values.vstring);
-                        ioffset = ioffset + value2;
+                        ioffset = check_offset_addition(ioffset, value2, sub2tree, pinfo, tvb);
                         ioffset += align_4(tvb, ioffset);
                     }
                     value1 = tvb_get_letohl(tvb, ioffset);  /* Optional Attributes */
@@ -5831,7 +5840,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
                         temp_values.vstring = get_string(tvb, ioffset, value2);
                         proto_tree_add_string(sub2tree, hf_mv_string, tvb, ioffset,
                                               value2, temp_values.vstring);
-                        ioffset = ioffset + value2;
+                        ioffset = check_offset_addition(ioffset, value2, sub2tree, pinfo, tvb);
                         ioffset += align_4(tvb, ioffset);
                     }
                     value1 = tvb_get_letohl(tvb, ioffset);    /* Default ACL */
@@ -5873,7 +5882,7 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
             values->vstring = get_string(tvb, ioffset, value2);
             proto_tree_add_string(ntree, hf_mv_string, tvb, ioffset,
                                   value2, values->vstring);
-            ioffset = ioffset + value2;
+            ioffset = check_offset_addition(ioffset, value2, ntree, pinfo, tvb);
         }
         if(tvb_captured_length_remaining(tvb, ioffset) < 4 )
         {
@@ -5890,14 +5899,14 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
             values->vstring = get_string(tvb, ioffset, value2);
             proto_tree_add_string(ntree, hf_nds_attribute_dn, tvb, ioffset,
                                   value2, values->vstring);
-            ioffset = ioffset + value2;
+            ioffset = check_offset_addition(ioffset, value2, ntree, pinfo, tvb);
             ioffset += align_4(tvb, ioffset);
             value2 = tvb_get_letohl(tvb, ioffset);  /* DN of Trustee */
             ioffset = ioffset + 4;
             values->vstring = get_string(tvb, ioffset, value2);
             proto_tree_add_string(ntree, hf_nds_trustee_dn, tvb, ioffset,
                                   value2, values->vstring);
-            ioffset = ioffset + value2;
+            ioffset = check_offset_addition(ioffset, value2, ntree, pinfo, tvb);
             ioffset += align_4(tvb, ioffset);
             proto_tree_add_item(ntree, hf_nds_privileges, tvb, ioffset, 4, ENC_LITTLE_ENDIAN);
             ioffset = ioffset + 4;
@@ -5918,14 +5927,14 @@ process_multivalues(proto_tree *ncp_tree, tvbuff_t *tvb, packet_info *pinfo, nds
             values->vstring = get_string(tvb, ioffset, value2);
             proto_tree_add_string(ntree, hf_nds_attribute_dn, tvb, ioffset,
                                   value2, values->vstring);
-            ioffset = ioffset + value2;
+            ioffset = check_offset_addition(ioffset, value2, ntree, pinfo, tvb);
             ioffset += align_4(tvb, ioffset);
             value2 = tvb_get_letohl(tvb, ioffset);  /* DN of Trustee */
             ioffset = ioffset + 4;
             values->vstring = get_string(tvb, ioffset, value2);
             proto_tree_add_string(ntree, hf_nds_trustee_dn, tvb, ioffset,
                                   value2, values->vstring);
-            ioffset = ioffset + value2;
+            ioffset = check_offset_addition(ioffset, value2, ntree, pinfo, tvb);
             ioffset += align_4(tvb, ioffset);
             value1 = tvb_get_letohl(tvb, ioffset);  /* Privileges */
             proto_tree_add_item(ntree, hf_nds_privileges, tvb, ioffset, 4, ENC_LITTLE_ENDIAN);
@@ -8768,7 +8777,7 @@ dissect_nds_request(tvbuff_t *tvb, packet_info *pinfo,
                              * prefix.
                              */
                             if (request_value)
-                                request_value->req_nds_flags = 
+                                request_value->req_nds_flags =
                                     DSI_ENTRY_ID|DSI_ENTRY_FLAGS|DSI_SUBORDINATE_COUNT|DSI_MODIFICATION_TIME|DSI_BASE_CLASS|DSI_ENTRY_RDN;
                         }
                         break;
diff --git a/tools/ncp2222.py b/tools/ncp2222.py
index a09a9e7650..2e56d1b310 100755
--- a/tools/ncp2222.py
+++ b/tools/ncp2222.py
@@ -6515,6 +6515,7 @@ static expert_field ei_ncp_effective_rights = EI_INIT;
 static expert_field ei_ncp_server = EI_INIT;
 static expert_field ei_ncp_invalid_offset = EI_INIT;
 static expert_field ei_ncp_address_type = EI_INIT;
+static expert_field ei_ncp_value_too_large = EI_INIT;
 """)
 
     # Look at all packet types in the packets collection, and cull information
@@ -8544,6 +8545,7 @@ proto_register_ncp2222(void)
         { &ei_ncp_no_request_record_found, { "ncp.no_request_record_found", PI_SEQUENCE, PI_NOTE, "No request record found.", EXPFILL }},
         { &ei_ncp_invalid_offset, { "ncp.invalid_offset", PI_MALFORMED, PI_ERROR, "Invalid offset", EXPFILL }},
         { &ei_ncp_address_type, { "ncp.address_type.unknown", PI_PROTOCOL, PI_WARN, "Unknown Address Type", EXPFILL }},
+        { &ei_ncp_value_too_large, { "ncp.value_too_large", PI_MALFORMED, PI_ERROR, "Length value goes past the end of the packet", EXPFILL }},
     };
 
     expert_module_t* expert_ncp;
-- 
2.30.2

